import socket
import json

class ClienteSocket:
	
	ENCODE = "UTF-8"
	HOST = '127.0.0.1'   # Endereco IP do Servidor
	PORT = 5000          # Porta que o Servidor esta
	MAX_BYTES = 65535    # Quantidade de Bytes a serem ser recebidos

	def __init__(self):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	
	def envia(self, dados):
		dados_string = json.dumps(dados).encode('utf-8')
		dest = (self.HOST, self.PORT)                                     # Define IP de origem e Porta de destino  
		self.sock.sendto(dados_string, dest)

	def recebe(self):
		data, address = self.sock.recvfrom(self.MAX_BYTES)
		return json.loads(data)
	
	def fechaSocket(self):
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Inicializar um socket UDP