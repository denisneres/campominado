import socket
from datetime import datetime
from mensagemJogo import *
from campominado import *

import json


ENCODE = "UTF-8"
MAX_BYTES = 65535
PORT = 5000            # Porta que o Servidor esta
HOST = '127.0.0.1'     	       # Endereco IP do Servidor

mensagensJogo = MensagemJogo()
campo = CampoMinado()

def server():
    #Abrindo um socket UDP na porta 5000
    orig = (HOST, PORT)																
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(orig)

    while True:
        #recebi dados
        data, address = sock.recvfrom(MAX_BYTES) # Recebi dados do socket
        data_loaded = json.loads(data.decode('utf-8'))
        if(data_loaded['escolha'] == 'iniciaJogo'):
            print("Iniciando o jogo")
            
            mensagensJogo = MensagemJogo()
            campo.setaTamanhoCampoMinado()
            campo.inicializaMatriz()

            #Envia resposta
            text = "Total de dados recebidos: " + str(len(data))
            retorno = {"idJogo":"jogodenis", "jogo":campo.getTabelaExibicao(), 'status':'continua', 'numJogadas':5} 
            data = json.dumps(retorno).encode('utf-8') # Codifica para BASE64 os dados 
            sock.sendto(data, address) # Enviando dados	
        
        elif(data_loaded['escolha'] == 'continua'):
            print("jogo continuando")
            alive = campo.escolheArea(data_loaded['posx'],data_loaded['posy'])
           
            print("posicao escolhida: ", data_loaded['posx'] , data_loaded['posy'])
            
            if(campo.getJogadasRestantes() == 0):
                status= 'venceu'
            elif(alive):
                status = 'continua'
            else:
                status = 'gameover'


            retorno = {"idJogo":"jogodenis", "jogo":campo.getTabelaExibicao(), 'status':status, 'numJogadas': str(campo.getJogadasRestantes())} 
            data = json.dumps(retorno).encode('utf-8') # Codifica para BASE64 os dados 
            sock.sendto(data, address) # Enviando dados	

        else:
            print("n deu certo passa dict")

        
