from datetime import datetime
from cliente_socket import *

def printaTabelaExibicao(tabela):
		for i in range(5):
			for j in range(5):
				print(tabela[i][j], end=' ')
			print("\n")

def printaTabelaExibicao(tabela, tamanho):
		for i in range(tamanho):
			for j in range(tamanho):
				print(tabela[i][j], end=' ')
			print("\n")

def verificaValoresXY(x,y, tabela):
    x_int = int(x)
    y_int = int(y)
    if(x_int<=4 and x_int>=0 and y_int<=4 and y_int>=0 and tabela[x_int][y_int] == '_'):
        return True
    else:
        print("Valores de [x,y] inválidos.")
        
    return False


def client():
    clienteSocket = ClienteSocket()

    name = input("Digite seu nome:\n")
    dados = {"escolha":"iniciaJogo", "id":name}
    clienteSocket.envia(dados)
    continua = True

    while(continua):

        situacaoJogo = clienteSocket.recebe()   

        if(situacaoJogo['status'] == 'gameover'):
            print("game over!!!")
            continua = False

        elif(situacaoJogo['status'] == 'venceu'):
            print("VOCE VENCEU")
            continua = False
        
        elif(situacaoJogo['status'] == 'continua'):
            printaTabelaExibicao(situacaoJogo['jogo'],5)
            posx = ''
            poxy = ''
            
            validadoXY = False
            while(validadoXY == False):
                print("Restam " + str(situacaoJogo['numJogadas']) + " jogadas para vencer:")
                posx = input("digite valores x: ")
                posy = input("digite valores y: ")

                validadoXY = verificaValoresXY(posx, posy, situacaoJogo['jogo'])
                

            dados = {"escolha":"continua", "id":name, "posx":posx, "posy":posy}
            clienteSocket.envia(dados)
        
        else:
            print("deu ruim")

    #Fechando Socket
    clienteSocket.fechaSocket()